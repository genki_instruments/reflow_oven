#include <MAX6675.h>
#include <LiquidCrystal_I2C.h>
#include <Wire.h>

/*
  Single_Temp.pde - Example using the MAX6675 Library.
  Created by Ryan McLaughlin <ryanjmclaughlin@gmail.com>

  This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License.
  http://creativecommons.org/licenses/by-sa/3.0/
*/


#define I2C_ADDR 0x27
#define BACKLIGHT_PIN 3

#define En_pin 2  // PIN  6 of circuit PCF8574  (Named P2) 

#define Rw_pin 1  // PIN  5 of circuit PCF8574  (Named P1)

#define Rs_pin 0  // PIN  4 of circuit PCF8574  (Named P0) 

#define D4_pin 4  // PIN  9 of circuit PCF8574  (Named P4)  

#define D5_pin 5  // PIN 10 of circuit PCF8574  (Named P5)  

#define D6_pin 6  // PIN 11 of circuit PCF8574  (Named P6)  

#define D7_pin 7  // PIN 12 of circuit PCF8574  (Named P7) 

LiquidCrystal_I2C lcd(I2C_ADDR,En_pin,Rw_pin,Rs_pin,D4_pin,D5_pin,D6_pin,D7_pin); 


int LED1 = 9;             // Status LED Pin
int CS = A3;             // CS pin on MAX6675
int SO = 12;              // SO pin of MAX6675
int thermoSCK = 13;             // thermoSCK pin of MAX6675
int units = 2;            // Units to readout temp (0 = raw, 1 = ˚C, 2 = ˚F)
float temperature = 0.0;  // Temperature output variable


// Initialize the MAX6675 Library for our chip
MAX6675 temp(CS,SO,thermoSCK,units);

int button = 4;
int relayPin = 9;

// Setup Serial output and LED Pin  
// MAX6675 Library already sets pin modes for MAX6675 chip!
void setup() {
  
  pinMode(button,INPUT);
  
  lcd.begin(16,2); 
        lcd.home ();
        lcd.print("OK");
        delay(1000);
        
}

void loop() {
  lcd.home();
  
  while(digitalRead(button)) {
      digitalWrite(relayPin,HIGH);
  }
      
  
        
        
	// Read the temp from the MAX6675
	temperature = temp.read_temp();

    lcd.print(temperature);
 
	// Wait one second before reading again
	delay(1000);
}
