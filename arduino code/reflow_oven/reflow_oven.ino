/*
 * Genki Instruments, 04/03/16
 * GenkiReflow v1.0
 * 
 * See attached pinout.txt for programming the oven with
 * an Arduino.
 */
 
#include <PinChangeInt.h>
#include <MAX6675.h>
#include <LiquidCrystal_I2C.h>
#include <Wire.h>
#include <PID_v1.h>

// LCD parameters
#define I2C_ADDR 0x27
#define En_pin 2  // PIN  6 of circuit PCF8574  (Named P2) 
#define Rw_pin 1  // PIN  5 of circuit PCF8574  (Named P1)
#define Rs_pin 0  // PIN  4 of circuit PCF8574  (Named P0) 
#define D4_pin 4  // PIN  9 of circuit PCF8574  (Named P4)  
#define D5_pin 5  // PIN 10 of circuit PCF8574  (Named P5)  
#define D6_pin 6  // PIN 11 of circuit PCF8574  (Named P6)  
#define D7_pin 7  // PIN 12 of circuit PCF8574  (Named P7) 

LiquidCrystal_I2C lcd(I2C_ADDR, En_pin, Rw_pin, Rs_pin, D4_pin, D5_pin, D6_pin, D7_pin);

// Thermocouple parameters
int CS = A3;              // CS pin on MAX6675
int SO = 12;              // SO pin of MAX6675
int thermoSCK = 13;       // thermoSCK pin of MAX6675
int units = 2;            // Units to readout temp (0 = raw, 1 = ËšC, 2 = ËšF)
double input = 0.0;       // Temperature output variable, PID input value

MAX6675 thermo(CS, SO, thermoSCK, units);


// PID parameters
// ** PRE-HEAT STAGE **
#define PID_KP_PREHEAT 100
#define PID_KI_PREHEAT 0.025
#define PID_KD_PREHEAT 20

// ** SOAKING STAGE **
#define PID_KP_SOAK 200
#define PID_KI_SOAK 0.015
#define PID_KD_SOAK 50

// ** REFLOW STAGE **
#define PID_KP_REFLOW 175
#define PID_KI_REFLOW 0.025
#define PID_KD_REFLOW 25
#define PID_SAMPLE_TIME 1000


// ***** LCD MESSAGES *****
const char* lcdMessagesReflowStatus[] = {
  "Ready",
  "Pre-heat",
  "Soak",
  "Reflow",
  "Cool",
  "Complete",
  "Wait, hot!",
  "Error"
};

// Timing parameters
unsigned long timeSinceStart;
unsigned long timeStart;


// PID control variables
double setpoint;
double output;
double kp = PID_KP_PREHEAT;
double ki = PID_KI_PREHEAT;
double kd = PID_KD_PREHEAT;

PID reflowOvenPID(&input, &output, &setpoint, kp, ki, kd, DIRECT);

// Type definitions
typedef enum REFLOW_STATE
{
  REFLOW_STATE_IDLE,
  REFLOW_STATE_PREHEAT,
  REFLOW_STATE_SOAK,
  REFLOW_STATE_REFLOW,
  REFLOW_STATE_COOL,
  REFLOW_STATE_COMPLETE,
  REFLOW_STATE_TOO_HOT,
  REFLOW_STATE_ERROR,
  REFLOW_STATE_OFF
} 
reflowState_t;

typedef enum REFLOW_STATUS
{
  REFLOW_STATUS_OFF,
  REFLOW_STATUS_ON
} 
reflowStatus_t;


// ***** CONSTANTS *****
int TEMPERATURE_REFLOW_MAX = 250;
int TEMPERATURE_REFLOW_PROFILE = 0;

#define TEMPERATURE_ROOM 50
#define TEMPERATURE_SOAK_MIN 140
#define TEMPERATURE_SOAK_MAX 183
#define TEMPERATURE_COOL_MIN 100
#define SENSOR_SAMPLING_TIME 1000
#define SOAK_TEMPERATURE_STEP 2.5
#define SOAK_MICRO_PERIOD 9000
#define DEBOUNCE_PERIOD_MIN 50
#define LCD_REFRESH_TIME 1000;

// Variables
int buttonSelect = 4;
int buttonStart = 3;
int buttonStop = 2;
int relayPin = 9;
int ledPin = 7;
int buzzerPin = 5;
int currentProfile = 0;
reflowState_t reflowState;
reflowStatus_t reflowStatus;
long nextCheck;
long nextRead;
long nextRefresh;
int selectedButton;
int lcdNewUpdate = 1;
int windowSize;
unsigned long windowStartTime;
unsigned long timerSoak;


void setup() {

  pinMode(buttonSelect, INPUT);
  PCintPort::attachInterrupt(buttonSelect, &interruptFunc, RISING);
  pinMode(buttonStart, INPUT);
  PCintPort::attachInterrupt(buttonStart, &interruptFunc, RISING);
  pinMode(buttonStop, INPUT);
  PCintPort::attachInterrupt(buttonStop, &interruptFunc, RISING);
  pinMode(relayPin, OUTPUT);
  pinMode(ledPin, OUTPUT);
  
  // Define buzzer pin as output
  pinMode(buzzerPin, OUTPUT);
  
  // Make sure relay is OFF
  digitalWrite(relayPin, LOW);
  
  // Make sure the initial statuse of the owen is OFF
  reflowStatus = REFLOW_STATUS_OFF;
  reflowState = REFLOW_STATE_OFF;

  // Initialize LCD display
  lcd.begin(16, 2); // column x row
  lcd.home();
  lcd.print("GenkiReflow");
  lcd.setCursor(0, 1);
  lcd.print("Version 1.0");

  nextCheck = millis();
  nextRead = millis();
  nextRefresh = millis();


  delay(2000);

  lcd.clear();
  lcd.home();
  
  // Initialize the windowSize to 2 sec
  windowSize = 2000;
}

// Interrupt function for input buttons
void interruptFunc() {
  delay(100);
  if (digitalRead(PCintPort::arduinoPin)) {
    selectedButton = PCintPort::arduinoPin;
  }
  else {
    selectedButton = -1;
  }
}

void update_LCD(String top, String bottom) {
  lcd.clear();
  lcd.home();
  lcd.print(top);
  lcd.setCursor(0, 1);
  lcd.print(bottom);
}

void loop() {
  unsigned long now = millis();

  // Read temperature from thermocouple if the time has come
  if (millis() > nextRead)  {
    nextRead = millis() + SENSOR_SAMPLING_TIME;
    input = (thermo.read_temp() - 32) / 1.8;
  }

  if (lcdNewUpdate) {
    String top =  "Select Profile";
    String bottom = "Profile " + String(225 + 10*currentProfile);
    update_LCD(top, bottom);

    lcdNewUpdate = 0;
  }
  
  if (selectedButton) {
    if (selectedButton == buttonSelect && reflowStatus == REFLOW_STATUS_OFF) {
      currentProfile = (currentProfile + 1) % 4;
      String top = "Select Profile";
      String bottom = "Profile " + String(225 + 10*currentProfile);
      update_LCD(top, bottom);
    }
    else if (selectedButton == buttonStart && reflowStatus == REFLOW_STATUS_OFF) {
      reflowStatus = REFLOW_STATUS_ON;
      reflowState = REFLOW_STATE_IDLE;
      switch(currentProfile) {
        case 0:
          TEMPERATURE_REFLOW_MAX = 225;
          break;
        case 1:
          TEMPERATURE_REFLOW_MAX = 235;
          break;
        case 2:
          TEMPERATURE_REFLOW_MAX = 245;
          break;
        case 3:
          TEMPERATURE_REFLOW_MAX = 255;
          break;
      }
      String top = "START";
      String bottom = "";
      update_LCD(top, bottom);
      delay(1000);
    }
    else if (selectedButton == buttonStop && reflowStatus == REFLOW_STATUS_ON) {
      reflowStatus = REFLOW_STATUS_OFF;
      reflowState = REFLOW_STATE_OFF;
      String top = "STOP";
      String bottom = "";
      update_LCD(top, bottom);
      delay(1000);

      top = "Select Profile";
      bottom = "Profile " + String(220 + 10*currentProfile);
      update_LCD(top, bottom);
    }
    selectedButton = -1;
  }
  
  // Reflow oven controller state machine
  switch (reflowState) {
    case REFLOW_STATE_IDLE:
      // If oven temperature is still above room temperature
      if (input >= TEMPERATURE_ROOM) {
        reflowState = REFLOW_STATE_TOO_HOT;
      } 
      else {
        // Start time
        timeStart = millis();
        // Initialize PID control window starting time
        windowStartTime = timeStart;
        // Ramp up to minimum soaking temperature
        setpoint = TEMPERATURE_SOAK_MIN;
        // Tell the PID to range between 0 and the full window size
        reflowOvenPID.SetOutputLimits(0, windowSize);
        reflowOvenPID.SetSampleTime(PID_SAMPLE_TIME);
        // Turn the PID on
        reflowOvenPID.SetMode(AUTOMATIC);
        // Proceed to preheat stage
        reflowState = REFLOW_STATE_PREHEAT;
        tone(buzzerPin, 440, 500);
      }
      break;

    case REFLOW_STATE_PREHEAT:
      reflowStatus = REFLOW_STATUS_ON;
      timeSinceStart = millis();
      // If minimum soak temperature is achieve
      if (input >= TEMPERATURE_SOAK_MIN)
      {
        // Chop soaking period into smaller sub-period
        timerSoak = millis() + SOAK_MICRO_PERIOD;
        // Set less agressive PID parameters for soaking ramp
        reflowOvenPID.SetTunings(PID_KP_SOAK, PID_KI_SOAK, PID_KD_SOAK);
        // Ramp up to first section of soaking temperature
        setpoint = TEMPERATURE_SOAK_MIN + SOAK_TEMPERATURE_STEP;
        // Proceed to soaking state
        reflowState = REFLOW_STATE_SOAK;
        tone(buzzerPin, 440, 500);
      }
      break;

    case REFLOW_STATE_SOAK:
      // If micro soak temperature is achieved
      if (millis() > timerSoak)
      {
        timerSoak = millis() + SOAK_MICRO_PERIOD;
        // Increment micro setpoint
        setpoint += SOAK_TEMPERATURE_STEP;
        if (setpoint > TEMPERATURE_SOAK_MAX)
        {
          // Set agressive PID parameters for reflow ramp
          reflowOvenPID.SetTunings(PID_KP_REFLOW, PID_KI_REFLOW, PID_KD_REFLOW);
          // Ramp up to first section of soaking temperature
          setpoint = TEMPERATURE_REFLOW_MAX;
          // Proceed to reflowing state
          reflowState = REFLOW_STATE_REFLOW;
          tone(buzzerPin, 440, 500);
        }
      }
      break;

    case REFLOW_STATE_REFLOW:
    // The only change the reflowProfile parameter has is on the MAX temp
    // 0: 220, 1: 230, 2: 240, 3: 250
    
      // We need to avoid hovering at peak temperature for too long
      // Crude method that works like a charm and safe for the components
      if (input >= (TEMPERATURE_REFLOW_MAX - 5))
      {
        // Set PID parameters for cooling ramp
        // reflowOvenPID.SetTunings(PID_KP_REFLOW, PID_KI_REFLOW, PID_KD_REFLOW);
        // Ramp down to minimum cooling temperature
        setpoint = TEMPERATURE_COOL_MIN;
        // Proceed to cooling state
        reflowState = REFLOW_STATE_COOL;
        tone(buzzerPin, 440, 500);
      }
      break;

    case REFLOW_STATE_COOL:
      // If minimum cool temperature is achieve
      if (input <= TEMPERATURE_COOL_MIN)
      {
        // Turn off reflow process
        reflowStatus = REFLOW_STATUS_OFF;
        // Proceed to reflow Completion state
        reflowState = REFLOW_STATE_COMPLETE;
        tone(buzzerPin, 440, 500);
      }
      break;

    case REFLOW_STATE_COMPLETE:
      // Reflow process ended
      reflowState = REFLOW_STATE_OFF;
      lcdNewUpdate = 1;
      break;

    case REFLOW_STATE_TOO_HOT:
      // If oven temperature drops below room temperature
      if (input < TEMPERATURE_ROOM)
      {
        // Ready to reflow
        reflowState = REFLOW_STATE_IDLE;
        lcdNewUpdate = 1;
      }
      break;
     
    default:
      break;
    }

  // PID computation and SSR control
  if (reflowStatus == REFLOW_STATUS_ON) {
    // Refresh LCD every second
    if(millis() > nextRefresh) {
      nextRefresh = millis() + LCD_REFRESH_TIME;
      timeSinceStart = millis();
      update_LCD("State: " + String(lcdMessagesReflowStatus[reflowState]), "T: " + String((int)input) + (char)223 + " t: " + (int) ((timeSinceStart - timeStart)/1000) + "s");
    }

    now = millis();
    reflowOvenPID.Compute();

    if ((now - windowStartTime) > windowSize) {
      // Time to shift the Relay Window
      
      
      windowStartTime += windowSize;
    }
    if (output > (now - windowStartTime)) {
      digitalWrite(ledPin, HIGH);
      digitalWrite(relayPin, HIGH);
    } 
    else {
      digitalWrite(ledPin, LOW);
      digitalWrite(relayPin, LOW);
    }
  }
  // Reflow oven process is off, ensure oven is off
  else {
    digitalWrite(ledPin, LOW);
    digitalWrite(relayPin, LOW);
  }

}



